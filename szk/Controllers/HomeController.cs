﻿using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using szk.Models;
using szk.Support;

namespace szk.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (var db = new ToiletModel())
            {
                var data = db.Toilets
                    .ToList()
                    .GroupBy(k => k.Floor)
                    .Select(g1 => new
                    {
                        Floor = g1.Key,
                        Count = g1.Count(),
                        Toilets = g1
                            .GroupBy(k => k.Type)
                            .Select(g2 => new
                            {
                                Type = g2.Key,
                                Count = g2.Count(),
                                Toilets = g2
                                    .Select(t => new
                                    {
                                        Id = t.Id,
                                        ToiletNo = t.ToiletNo,
                                        State = t.State,
                                        LastChange = TimeZoneInfo.ConvertTime(t.LastChange, TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(t.Zone))
                                    }).OrderBy(t => t.ToiletNo)
                                    .Select(t => t.ToExpando())
                                    .ToList()
                            }).OrderBy(t => t.Type)
                            .Select(t => t.ToExpando())
                            .ToList()
                    }).OrderBy(t => t.Floor)
                    .Select(t => t.ToExpando())
                    .ToList();

                var count = db.Messages.Count();
                var index = new Random().Next(count);
                var message = db.Messages
                    .OrderBy(m => m.Id)
                    .Skip(index)
                    .First().Text;

                return View(new { Data = data, Message = message }.ToExpando());
            }
        }

        [HttpPost]
        public ActionResult Update(int id, int state)
        {
            using (var db = new ToiletModel())
            {
                var toilet = db.Toilets.Find(id);
                if (toilet == null)
                    throw new ArgumentOutOfRangeException("Toilet does not exist!");

                var oldState = toilet.State;

                toilet.State = (ToiletState)state;
                if (toilet.State != ToiletState.Normal && toilet.State != ToiletState.Medium && toilet.State != ToiletState.Extreme)
                    throw new ArgumentOutOfRangeException("Wrong toilet state!");

                toilet.LastChange = DateTime.UtcNow;

                db.Entry(toilet).CurrentValues.SetValues(toilet);
                db.SaveChanges();

                TwitterClient.Instance.TweetToiletState(toilet, oldState);

                var now = TimeZoneInfo.ConvertTime(toilet.LastChange, TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(toilet.Zone));
                return Json(new { LastChange = now.ToString("yyyy-MM-dd HH:mm") });
            }
        }
    }
}