﻿using LinqToTwitter;
using System.Threading.Tasks;
using szk.Models;

namespace szk.Support
{
    public class TwitterClient
    {
        private static readonly TwitterClient _instance = new TwitterClient(new TwitterSettings());
        public static TwitterClient Instance { get { return _instance; } }

        private readonly SingleUserAuthorizer _authorization;

        private readonly TweetFormatter _formatter = new TweetFormatter();

        private TwitterClient(TwitterSettings settings)
        {
            _authorization = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = settings.ConsumerKey,
                    ConsumerSecret = settings.ConsumerSecret,
                    AccessToken = settings.AccessToken,
                    AccessTokenSecret = settings.AccessTokenSecret
                }
            };
        }

        public void TweetToiletState(Toilet toilet, ToiletState oldState)
        {
            if (toilet.State != oldState)
            {
                var context = new TwitterContext(_authorization);
                Task.Run(async () =>
                {
                    try
                    {
                        await context.TweetAsync(_formatter.FormatTweet(toilet));
                    }
                    catch { /* yeah we have swallowd it */ }
                });
            }
        }
    }
}