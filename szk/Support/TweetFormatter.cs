﻿using szk.Models;

namespace szk.Support
{
    public class TweetFormatter
    {
        static string[] FLOORS =
        {
            "у приземљу",
            "на првом спрату",
            "на другом спрату",
            "на трећем спрату",
            "на четвртом спрату",
            "на петом спрату",
            "на шестом спрату",
            "на седмом спрату",
            "на осмом спрату",
            "на деветом спрату",
            "на десетом спрату"
        };

        static string[] TOILETS =
        {
            "женском клозету",
            "мушком клозету",
            "клозету за инвалиде"
        };

        public string FormatTweet(Toilet toilet)
        {
            switch (toilet.State)
            {
                case ToiletState.Normal:
                    return string.Format("Kлоња #{0} у {1} {2} је чиста к'о суза! #шзк", toilet.ToiletNo, TOILETS[(int)toilet.Type], FLOORS[toilet.Floor]);
                case ToiletState.Medium:
                    return string.Format("Kлоња #{0} у {1} {2} је кол'ко-тол'ко чиста! #шзк", toilet.ToiletNo, TOILETS[(int)toilet.Type], FLOORS[toilet.Floor]);
                case ToiletState.Extreme:
                    return string.Format("Неко је заср\'о клоњу #{0} у {1} {2}! #шзк #tweetthatshit", toilet.ToiletNo, TOILETS[(int)toilet.Type], FLOORS[toilet.Floor]);
                default:
                    return string.Format("Kлоња #{0} у {1} {2} је непознатом стању?! Нешто се сјебало!", toilet.ToiletNo, TOILETS[(int)toilet.Type], FLOORS[toilet.Floor]);
            }
        }
    }
}