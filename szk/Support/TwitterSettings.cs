﻿using System.Web.Configuration;

namespace szk.Support
{
    public class TwitterSettings
    {
        public string ConsumerKey { get; private set; }
        public string ConsumerSecret { get; private set; }
        public string AccessToken { get; private set; }
        public string AccessTokenSecret { get; private set; }

        public TwitterSettings()
        {
            ConsumerKey = WebConfigurationManager.AppSettings["TwitterConsumerKey"];
            ConsumerSecret = WebConfigurationManager.AppSettings["TwitterConsumerSecret"];
            AccessToken = WebConfigurationManager.AppSettings["TwitterAccessToken"];
            AccessTokenSecret = WebConfigurationManager.AppSettings["TwitterAccessTokenSecret"];
        }
    }
}