namespace szk.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ToiletModel : DbContext
    {
        // Your context has been configured to use a 'Model1' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'szk.Models.Model1' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Model1' 
        // connection string in the application configuration file.
        public ToiletModel()
            : base("name=ToiletModel")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Toilet> Toilets { get; set; }
        public virtual DbSet<DailyMessage> Messages { get; set; }
    }

    public enum ToiletType
    {
        Female,
        Male,
        Disabled
    }

    public enum ToiletState
    {
        Normal,
        Medium,
        Extreme
    }

    public class Toilet
    {
        public int Id { get; set; }

        public int Floor { get; set; }
        public ToiletType Type { get; set; }
        public int ToiletNo { get; set; }

        public ToiletState State { get; set; }

        public DateTime LastChange { get; set; }
        public string Zone { get; set; }
    }

    public class DailyMessage
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}