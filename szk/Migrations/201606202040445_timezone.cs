namespace szk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class timezone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Toilets", "Zone", c => c.String(nullable: false, defaultValue: "Central Europe Standard Time"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Toilets", "Zone");
        }
    }
}
