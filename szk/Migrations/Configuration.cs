﻿namespace szk.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<szk.Models.ToiletModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(szk.Models.ToiletModel context)
        {
            //context.Toilets.AddOrUpdate(t => t.Id,
            //    new Models.Toilet { Id = 0, Floor = 5, Type = Models.ToiletType.Female, ToiletNo = 1 });

            context.Messages.AddOrUpdate(m => m.Id,
                new Models.DailyMessage { Id = 0, Text = "ПОМОЗИ КОЛЕГАМА У НУЖДИ - АБДЕЈТУЈ СТАТУС!" },
                new Models.DailyMessage { Id = 1, Text = "ПРЕ И ПОСЛЕ СРАЊА ТРЕБА КЛОЊУ ПРАТИ, НЕМОЈ НА ТО ДА ТЕ ОПОМИЊЕ МАТИ" },
                new Models.DailyMessage { Id = 2, Text = "ИНФОРМАЦИЈЕ ПУТУЈУ БРЖЕ ОД СМРАДА" },
                new Models.DailyMessage { Id = 3, Text = "IF YOU SMELL SOMETHING, SAY SOMETHING" },
                new Models.DailyMessage { Id = 4, Text = "WHERE DO YOU WANT TO POOP TODAY?" });
        }
    }
}
