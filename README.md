# Прочитај ме - или немој #

Укратко о пројекту за неупућене.

### О пројекту ###

* ШЗК Пројекат - Nextgen платформа за праћење стања клоња по фирмама
* ШЗК је скраћеница од "Што си заср'о клоњу"
* Још увек немамо верзију

### Техничке ствари ###

* Платформа - .NET
* Језик - C#
* База - MS SQL Server (још увек немамо потребе за Oraclе-ом)
* Веб фрејмврк - ASP.NET MVC
* ДАЛ - Entity Framework
* Инструкције о подизању окружења биће ускоро доступне
* Неопходан ИДЕ - Visual Studio 2015 (може Community верзија)

Ово је била идеална прилика да искористим F#, али касно сам се сетио, а сад ме мрзи да поново правим Visual Studio пројекат.

### Твој допринос ###

* Тестови - јебеш тестове, важно је да ради
* Ривју кода - ако ради, ко смо ми да судимо
* Сви таскови су критикал (ако није критикал није вредно труда)

### Ко жели да помогне ###

* Знаће како да ме нађе